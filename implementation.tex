This section details why and how to adapted the four stages (Parsing, Name
analyzing, Type checking and Code generation) of the compiler pipeline to support
anonymous functions.

\subsection{Theoretical Background}

The aim of this extension is to make function first-class citizens, which means
being able to treat functions as any other variable. It implies the following
changes:

\begin{itemize}
  \item Parser: we need to support a specific synthax to define the type and
    definition of anonymous funtions.
  \item Name analyzer: anonymous functions do not have a name, but we need one
    internally and need to store the signature (argument and return types) for
    later use in the type checker and code generation. We also have to support
    calls to variables.
  \item Type checker: our pipeline uses a striped-down Hindley–Milner type
    system, which generate constraints and solve them using an unification
    algorithm. As a fully-fledged Hindley-Milner implementation does support
    anonymous function, we only have to complete our own.
  \item Code generation: the only additional theory would be closures or lambda
    lifting. Closures get a tuple containing all of the captured variables.
    Lambda lifted anonymous function receive their free variables as additional
    arguments.
\end{itemize}

The above changes are sufficient to support {\it closed} functions, which means
they do not use variable from the scope where they are defined. We looked into
two potential solutions to support non-closed function: closure conversion and
lambda lifting but did not implement them due to the time we could allocate to
this project.

\subsection{Implementation Details}

\subsubsection{Parser}

The changes to the parser are quite small as we only need to add a few entries
to the existing Amy synthax, which is handled by our generic parser. Note that
we use the {\it fn} keyword as a prefix to the definition of anonymous
function since it is the straighforward way to keep the synthax LL(1).

\subsubsection{Name analyzer}

The name analyzer is tricker to implement: we have to gather and keep track of
the somewaht complex structure of a function while moving it around as any
other variable. We have to detect variables annoted as {\it FunctionType} in
order to switch to the specific handling of anonymous function, which is done
by the {\it transformAnoFunDef} method: they are handled like functions and
constructors and their signatures are stored in a dedicated symbol table for
later use in the type checking stage. \\

We decided to keep track of the local functions in the existing {\it locals}
data structure to minimize the changes to unrelated sections of the code. This
choice force us to check whether the resolved variable is indeed annoted with
the {\it FunctionType} type from times to times but we believe it generates
less boilerplate in the end.

\subsubsection{Type checker}

Generating the constraints for the anonymous functions is easy: we resolve the
signature of the function from the symbol table and directly zip the argument
and return types with the expected values. There is no need to check if the
number of argument match the type annotation, as it already has been done
during the Name Analysis stage. \\

We solve the generated constraint using the unification algorithm discussed in
CS-310: we only implemented the {\it decompose function} and {\it occurs check}
steps as we already had a partial implementation. Note that the {\it occurs
  check} step is useless in the current state of the extension, since our
anonymous functions cannot use variables not defined in its parameters (i.e.
type resolution cannot loop forever).

\subsubsection{Code Generation}

We heavily relied on \cite[Mozilla's documentation on
WebAssembly]{website:mdm-webassembly} to design the necessary extensions to the
code generation stage. \\

We use a two-part system: first we generate the anonymous functions to place
them with the other function definitions. Then, when we reach the part where
the lambda is declared in the code, we place its index onto the stack. \\

This, however, required a way to have all the lambdas definitions at the very
beginning of our code generation (we couldn't place a function inside another
one in web assembly). To remedy this, we made the constructor of the
AnonymousFunction class private, and forced its creation through a helper
method. This helper method helped keep track of all instances of
AnonymousFunction. \\

Additionally, it gave the {\it AnonymousFunction} object an index in the table,
as well as a unique name (derived from its index). \\

We also had to update {\it wat2wasm}, because the version originally packaged
with the project does not support the latest standard. More specifically, {\it
  call\_indirect} required a predefined type. The current version supports the
ability to define parameter types and return type right at call. \\

With the packaged version, the followed was required:

\begin{lstlisting}
(type $param_f32_return_i32 (func (param f32)(result i32)))
(func (result i32)
  f32.const 3.14 ;; function parameter
  i32.const 0    ;; index of function in table
  call_indirect $param_f32_return_i32)
\end{lstlisting}

Whereas the up-to-date version can replace it by:

\begin{lstlisting}
(func (result i32)
  f32.const 3.14 ;; function parameter
  i32.const 0    ;; index of function in table
  call_indirect (param f32)(result i32))
\end{lstlisting}

Because type is a top-level instruction, it would've needed to be put in
ModulePrinter, which would've made it much clunkier than it needed to be. \\

Additionally, it gave the AnonymousFunction object an index in the table, as well
as a unique name (derived from its index).
